IDEAL is an algorithm to identify candidate miRNAs responsible for disease phenotype based on a Systems Biology/Network Biology approach. 

To install this package, run the following commands:

install.packages("devtools")
library("devtools")
install_gitlab("abhijeet095/IDEAL")
library("IDEAL")

On running the command:
ideal()
the package will run an example dataset. 

For more information about the documenation, please refer to the related publication:
Kılıç A, Santolini M, Nakano T, et al. A systems immunology approach identifies the collective impact of 5 miRs in Th2 inflammation. JCI Insight. 2018;3(11):e97503. Published 2018 Jun 7. doi:10.1172/jci.insight.97503

Proper documenting is under process. For any further information, contact me on abhijeet095@gmail.com

Cheers!

#' Rank miRNAs by decrease in LCC size for random and real miRNA-mRNA networks
#'
#' This function calls the function rank_by_FracLCC() function for real and random miRNA-mRNA
#' networks. The function takes as input the bipartite network as dataframes and the disease
#' module. The function merges the output of the function rank_by_FracLCC() and returns this
#' merged dataframe.
#'
#' @param G_mRNA_DE The disease module as an iGraph object
#' @param miRNAs_targets A dataframe having all pairs of miRNA and target mRNAs as rows
#' @param miRNAs_targets_r A list which contains random miRNA-mRNA networks
#' @param root cell type
#' @return A merged dataframe which contains the ranked miRNAs for real and random miRNA-mRNA networks
#' @export

compAdaptiveAttack_FracLCC<-function(G_mRNA_DE, miRNAs_targets, miRNAs_targets_r, root='Th2_chronic'){
  #get list for the observed network
  #get list for all the random networks
  for(i in 0:length(miRNAs_targets_r)){
    if(i==0){
      print('observed')
      df=rank_by_FracLCC(G_mRNA_DE, miRNAs_targets, 'observed')
      next
    }
    print(paste0('random_',as.character(i)))
    df=rbind(df, rank_by_FracLCC(G_mRNA_DE, miRNAs_targets_r[[i]], paste0('random_',as.character(i))))
  }
  write.csv(df,paste0("results/", root, '/ranking_by_FracLCC.csv'))
  return(df)
}

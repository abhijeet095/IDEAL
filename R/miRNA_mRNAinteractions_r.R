#' Generate random miRNA-mRNA bipartite networks
#'
#' This function records the number of targets for each miRNA and then assigns the same number
#' random targets out of all the target mRNAs. NRAND_mirnet number of such random miRNA-mRNA
#' bipartite networks are created and returned as a list
#'
#' @param NRAND_mirnet Number of random networks to be created
#' @param miRNAs_targets a dataframe having all pairs of miRNA and target mRNAs as rows
#' @param G_mRNA_DE some graph (will update)
#' @return a list which contains NRAND_mirnet number of random networks as dataframes of the form of miRNAs_targets
#' @export

miRNA_mRNAinteractions_r<-function(NRAND_mirnet, miRNAs_targets, G_mRNA_DE){
  miRNAs_targets_r=list()
  all_targs=unique(miRNAs_targets$target)
  all_targs=all_targs[!(all_targs=='')]
  for(i in 1:NRAND_mirnet){
    all_targs_r=sample(igraph::vertex_attr(G_mRNA_DE)$name, length(all_targs))
    #create d
    d=data.frame(matrix(0, nrow(miRNAs_targets), 2), stringsAsFactors = FALSE)
    colnames(d)=c('miRNA', 'target')
    d$miRNA=miRNAs_targets$miRNA
    d$target=unlist(lapply( unlist(lapply(split(d$miRNA, d$miRNA), function(y) length(y)))
                            , function(x) sample(all_targs_r, x)))
    d$target[miRNAs_targets$target=='']=''
    miRNAs_targets_r[[length(miRNAs_targets_r)+1]]=d
  }
  return(miRNAs_targets_r)
}

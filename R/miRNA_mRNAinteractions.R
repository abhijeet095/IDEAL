#' Determine miRNA targets
#'
#' This function takes in the list of miRNAs and mRNAs being considered for the analysis along
#' with the fold change (FC) expression values. It also takes as input a miRNA-mRNA bipartite network
#' as an iGraph object.
#'
#' This function selects the targets of miRNAs based on two criteria:
#'     1. The target should be present in mRNAs_FCs
#'     2. The product of FC value of the miRNA and the target should be opposite in sign
#'
#' The function returns a dataframe which has each miRNA-mRNA pair
#'
#' @param miRNAs_FCs a dataframe which contains the FC expression values for each miRNA
#' @param mRNAs_FCs a dataframe which contains the FC expression values for each mRNA
#' @param Gmir The miRNA-mRNA network as an iGraph object
#' @param Gp Graph object of netfull_PPI2013_TF
#' @return A dataframe having miRNAs in the first column and their target mRNAs in the second column.
#' miRNAs having multiple targets are presented as multiple rows having same miRNA but different target mRNAs.
#' miRNAs having no target mRNAs are presented as a single row with an empty cell in the mRNA column
#' @export

miRNA_mRNAinteractions<-function(miRNAs_FCs, mRNAs_FCs, Gmir, Gp){
  #for each miRNA in miRNA_FCs, generate miRNAs_Targets and miRNA_Targets_full
  miRNAs_targets=data.frame(matrix(0, 0, 2), stringsAsFactors = FALSE)
  miRNAs_targets_full=data.frame(matrix(0, 0, 2), stringsAsFactors = FALSE)
  colnames(miRNAs_targets)=c('miRNA', 'target')
  colnames(miRNAs_targets_full)=c('miRNA', 'target')
  count1=0
  count2=0
  count3=0
  for(miRNA in miRNAs_FCs$gene){
    fc=as.numeric(miRNAs_FCs$foldchange[miRNAs_FCs$gene==miRNA])
    #targs=neighbors(Gmir, miRNA)
    #targs=as.character(targs)
    targs=igraph::V(Gmir)$name[igraph::neighbors(Gmir, miRNA, mode = "total")]
    targs=targs[targs %in% mRNAs_FCs$gene]
    if(length(targs)==0){
      count1=count1+1
      miRNAs_targets_full[(nrow(miRNAs_targets_full)+1),]=c(miRNA, '')
      miRNAs_targets[(nrow(miRNAs_targets)+1),]=c(miRNA, '')
      next
    }
    targs=targs[fc*as.numeric(mRNAs_FCs$foldchange[unlist(lapply(targs, function(x) which(mRNAs_FCs$gene==x)))])<0]
    if(length(targs)==0){
      count2=count2+1
      miRNAs_targets_full[(nrow(miRNAs_targets_full)+1),]=c(miRNA, '')
      miRNAs_targets[(nrow(miRNAs_targets)+1),]=c(miRNA, '')
      next
    }
    #is it possible that a particular miRNA does not have any target
    miRNAs_targets_full[(nrow(miRNAs_targets_full)+1):(nrow(miRNAs_targets_full)+length(targs)),]=matrix(0,length(targs),2)
    miRNAs_targets_full$miRNA[(nrow(miRNAs_targets_full)-length(targs)+1):nrow(miRNAs_targets_full)]=rep(miRNA, length(targs))
    miRNAs_targets_full$target[(nrow(miRNAs_targets_full)-length(targs)+1):nrow(miRNAs_targets_full)]=targs
    targs=targs[targs %in% igraph::vertex_attr(Gp)$name]
    if(length(targs)==0){
      count3=count3+1
      miRNAs_targets_full[(nrow(miRNAs_targets_full)+1),]=c(miRNA, '')
      miRNAs_targets[(nrow(miRNAs_targets)+1),]=c(miRNA, '')
      next
    }
    miRNAs_targets[(nrow(miRNAs_targets)+1):(nrow(miRNAs_targets)+length(targs)),]=matrix(0,length(targs),2)
    miRNAs_targets$miRNA[(nrow(miRNAs_targets)-length(targs)+1):nrow(miRNAs_targets)]=rep(miRNA, length(targs))
    miRNAs_targets$target[(nrow(miRNAs_targets)-length(targs)+1):nrow(miRNAs_targets)]=targs
  }
  #print(miRNAs_targets)
  print(paste0("Nb of miRNAs with no targets : ", as.character(count1+count2+count3)))
  return(miRNAs_targets)
  #return(miRNAs_targets_full)
}

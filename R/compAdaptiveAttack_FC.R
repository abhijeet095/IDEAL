#' Rank miRNAs by fold change expression values for random and real miRNA-mRNA networks
#'
#' This function calls the function rank_by_FC() function for real and random miRNA-mRNA
#' networks. The function takes as input the bipartite network as dataframes and the disease
#' module. The function merges the output of the function rank_by_FC() and returns this
#' merged dataframe.
#'
#' @param G_mRNA_DE The disease module as an iGraph object
#' @param miRNAs_targets A dataframe having all pairs of miRNA and target mRNAs as rows
#' @param miRNAs_targets_r A list which contains random miRNA-mRNA networks
#' @param miRNAs_FCs a dataframe which contains the fold change values of each miRNA
#' @param root cell type
#' @return A merged dataframe which contains the ranked miRNAs for real and random miRNA-mRNA networks
#' @export

compAdaptiveAttack_FC<-function(G_mRNA_DE, miRNAs_targets, miRNAs_targets_r, miRNAs_FCs, root='Th2_chronic'){
  #get list for the observed network
  #get list for all the random networks
  for(i in 0:length(miRNAs_targets_r)){
    if(i==0){
      print('observed')
      df=rank_by_FC(G_mRNA_DE, miRNAs_targets, miRNAs_FCs, 'observed')
      next
    }
    print(paste0('random_',as.character(i)))
    df=rbind(df, rank_by_FC(G_mRNA_DE, miRNAs_targets_r[[i]], miRNAs_FCs, paste0('random_',as.character(i))))
  }
  write.csv(df,paste0('results/', root, '/ranking_by_FC.csv'))
  return(df)
}

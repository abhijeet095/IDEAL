---
title: "IDEAL Notebook"
output: html_notebook
---

We will first install and load the devtools and IDEAL package. 

```{r}
install.packages("devtools")
library(devtools)
install_gitlab("abhijeet095/IDEAL")
library(IDEAL)
```

Running the function 'ideal' performs the analysis for an example dataset.
```{r}
ideal()
```



